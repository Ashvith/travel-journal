import Navbar from './components/Navbar'
import List from './components/List'
import data from './assets/data/places.json'

function App() {
  const lists = data.map((item) => {
    return <List key={item.id} {...item} />
  })
  return (
    <div className='app'>
      <Navbar />
      <div className='list-group'>{lists}</div>
    </div>
  )
}

export default App
