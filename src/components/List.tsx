import Pin from '../assets/images/pin.png'

function List(props) {
  return (
    <div className='list'>
      <img src={props.imageUrl} className='list--image' />
      <div className='list--about'>
        <div className='list--location'>
          <img src={Pin} className='pin' />
          <span className='list--location--name'>
            {props.location.toUpperCase()}
          </span>
          <a href={props.googleMapsUrl} className='list--location--map'>
            View on Google Maps
          </a>
        </div>
        <div className='list--name'>{props.title}</div>
        <div className='list--date'>
          {props.startDate} - {props.endDate}
        </div>
        <div className='list--description'>{props.description}</div>
      </div>
    </div>
  )
}

export default List
