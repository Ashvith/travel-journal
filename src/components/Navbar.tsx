import Logo from '../assets/images/logo.png'
function Navbar() {
  return (
    <nav className='nav'>
      <img src={Logo} className='nav--logo' />
      <span className='nav--title'>my travel journal.</span>
    </nav>
  )
}

export default Navbar
